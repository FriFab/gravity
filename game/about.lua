local composer = require ("composer")
local scene = composer.newScene()
local widget = require("widget")
local loader= require ("loader")

local Cx=display.contentCenterX
local Cy=display.contentCenterY

local backText
local description1
local description2
local giuseppe
local fabiano
local andrea

local counter=0


local function backToMenu()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene("menu")
  composer.removeScene("about")
end

local function unlock()
 if counter>=2 then
    local value=""
    for i=0, levelMax-1 do
      value=value.."1"
    end

  loader.saveValue("levels",value)
 end
 counter=counter+1

end



function scene:create(event)

  local sceneGroup=self.view

  local background=display.newImageRect("background.png",4000,3000)

  backText=display.newText("Back",display.contentWidth-110,display.contentHeight-110,"orangeJuice.ttf",{fontSize=50})
  backText.size=100
  backText:setFillColor(0,0,0)

  description1=display.newText("Mobile computing",Cx,100,"orangeJuice.ttf",{fontSize=50})
  description1.size=90
  description1:setFillColor(0,0,0)

  description2=display.newText("project 2017",Cx,200,"orangeJuice.ttf",{fontSize=50})
  description2.size=90
  description2:setFillColor(0,0,0)

  giuseppe=display.newText("Giuseppe Matera", Cx,Cy,"orangeJuice.ttf",{fontSize=50})
  giuseppe.size=70
  giuseppe:setFillColor(0,0,0)

  fabiano=display.newText("Fabiano Friscia", Cx,Cy+100,"orangeJuice.ttf",{fontSize=50})
  fabiano.size=70
  fabiano:setFillColor(0,0,0)

  andrea=display.newText("Andrea Vona", Cx,Cy+200,"orangeJuice.ttf",{fontSize=50})
  andrea.size=70
  andrea:setFillColor(0,0,0)



  local backBtn=widget.newButton{ 
  width=200,
  height=100,
  defaultFile="nil.png", 
  onRelease = backToMenu
  }
  backBtn.x=display.contentWidth-100
  backBtn.y=display.contentHeight-100

  local unlockBtn=widget.newButton{ 
  width=200,
  height=100,
  defaultFile="nil.png", 
  onRelease = unlock
  }
  unlockBtn.x=Cx
  unlockBtn.y=Cy

  sceneGroup:insert(background)
  sceneGroup:insert(backBtn)
  sceneGroup:insert(unlockBtn)
  sceneGroup:insert(backText)
  sceneGroup:insert(description1)
  sceneGroup:insert(description2)
  sceneGroup:insert(giuseppe)
  sceneGroup:insert(fabiano)
  sceneGroup:insert(andrea)

end





function scene:show(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
     
  elseif phase == "did" then
 
  end 

end




function scene:hide(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
    
     
  elseif phase == "did" then

    
  end 

end




function scene:destroy(event)
  local sceneGroup = self.view

  print("about scene removed")

end




scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene