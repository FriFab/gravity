local composer = require ("composer")
local scene = composer.newScene()
local widget=require ("widget")

local physics=require("physics")
physics.start()
physics.setScale(60)
physics.setDrawMode("normal")

local game=require("gameHandler") --gameHandler gestisce le dinamiche di vincita e perdita e altre cose comuni a tutti i livelli
local draw=require("Draw") --modulo per disegnare linee

local tiled = require "com.ponywolf.ponytiled" --copiare file ponytiled.lua in com/ponywolf
local mapData 
local map 

local background
local downWall
local leftWall
local rightWall
local topWall

local Cx=display.contentCenterX 
local Cy=display.contentCenterY
local W=display.contentWidth
local H=display.contentHeight

local spike1
local spike2
local pla1
local pla2
local pla3
local plb1
local plb2
local plb3
local board


local start
local backBtn
local eraserBtn



  

 
local function onCollision(self, event)
 if event.phase=="began" and self.myName=="ball" then
   audio.play(game.bounceSound, {channel=31})

 elseif (self.myName=="ball" or self.myName=="glass") and event.other.name=="board" then
  	audio.play(game.boingSound, {channel=5})
  	board:play()
 end

 if event.phase=="began" and self.myName=="glass" and event.other.myName=="ball"
    and event.other.x-35 >self.x-65 and event.other.x+35 <self.x+65 then
    if not game.lost then
        game:win()
      end
    display.remove(game.ball)
 
 elseif (event.other.name=="top" or event.other.name=="left" or event.other.name=="right"
        or event.other.name=="down" or event.other.name=="spike1" or event.other.name=="spike2") and (not game.lost) and (not game.won) and self.myName=="ball" then
game:lose()

 end
     
end   

local function startGame() 
  physics.addBody(game.ball,"dynamic",{density=0.8,friction=0.4, bounce=0.6,radius=40})
  physics.addBody(game.glass,"dynamic",{shape=game.glassShape, density=1.4,friction=0.7})
  Runtime:removeEventListener("touch", draw)
  display.remove(start)
  display.remove(eraserBtn)

end

local function pause()
audio.play(buttonSound, {channel=32})
physics.pause()
Runtime:removeEventListener("touch", draw)
composer.gotoScene("pauseMenu")

end

local function erase()
   draw:change()
end 
 

function scene:create(event)


local sceneGroup=self.view

physics.setGravity(0,9.8)

game:new()

audio.play(game.gameSong, {loops=-1, channel=2})

game.ball=display.newImageRect("bubble.png",80,80) 

mapData= require "lvl2-map" -- load from lua export
map= tiled.new(mapData) -- look for images in lua export
game:setMap(map)


game.ball.x=100
game.ball.y=100
game.ball.myName="ball"

game.glass=display.newImageRect("glass.png",137.37,185.81)
game.glass.x=W-500
game.glass.y=H-93
game.glass.myName="glass"

eraserBtn=game.eraseBtn

start=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="playButton.png", 
    onRelease=startGame
  }
backBtn=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="backButton.png", 
    onRelease=pause
  }

eraserBtn = widget.newSwitch(
    {
        x = W-75,
        y = 185,
        style = "checkbox",
        width = 64,
        height = 64,
        onPress = erase,
        sheet = game.eraserSheet,
        frameOff = 2,
        frameOn = 1
    }
)


 start.x=W-75
 start.y=75

 backBtn.x=75
 backBtn.y=H-75

game.nextLvl=display.newImageRect("nil.png",400,100)
game.nextLvl.x=Cx
game.nextLvl.y=Cy+150



--Create screen boundaries
 leftWall = display.newRect(0,0,1, display.actualContentHeight*2 )
 rightWall = display.newRect (display.actualContentWidth, 0, 1, display.actualContentHeight*2)
 topWall = display.newRect (0, 0, display.actualContentWidth*2, 1)
 downWall=display.newRect(0,display.actualContentHeight,display.actualContentWidth*2,1)

--Invisible walls
downWall.alpha=0
topWall.alpha=0
rightWall.alpha=0
leftWall.alpha=0

downWall.name="down"
topWall.name="top"
rightWall.name="right"
leftWall.name="left"

--Sprite (vedere gameHandler per maggiori info)
board=display.newSprite(game.boardsheet, game.boardSequenceData)

board.x=385
board.y=945
board.name="board"

--Map objects
spike1=map:findObject("spike1")
spike2=map:findObject("spike2")
pla1=map:findObject("pla1") --Bisognerebbe accorparli in un unico oggetto
pla2=map:findObject("pla2")
pla3=map:findObject("pla3")
plb1=map:findObject("plb1") --Bisognerebbe accorparli in un unico oggetto
plb2=map:findObject("plb2")
plb3=map:findObject("plb3")



sceneGroup:insert(map)
sceneGroup:insert(game.winText)
sceneGroup:insert(game.loseText)
sceneGroup:insert(game.nextText)
sceneGroup:insert(game.ball)
sceneGroup:insert(game.glass)
sceneGroup:insert(game.nextLvl)
sceneGroup:insert(board)
sceneGroup:insert(start)
sceneGroup:insert(backBtn)
sceneGroup:insert(eraserBtn)



  physics.addBody (leftWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (rightWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (topWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody(downWall, "static", {density=0, friction=0.5, bounce=0.3}) 
  physics.addBody(spike1,"static", {density=0, friction=0.5, bounce=0, shape=spikeShape})
  physics.addBody(spike2,"static", {density=0, friction=0.5, bounce=0, shape=spikeShape})
  physics.addBody(pla1,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(pla2,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(pla3,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(plb1,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(plb2,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(plb3,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(board,"static", {density=0, friction=0.5, bounce=2, shape=game.boardShape})

  draw:newLine()

  sceneGroup:insert(draw:setInk(60))

  game.ball.collision=onCollision
  game.ball:addEventListener("collision",game.ball)
  
  
  game.glass.collision=onCollision
  game.glass:addEventListener("collision",game.glass)

  end 



function scene:show( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if phase == "will" then
   draw:setOn()
   Runtime:addEventListener("touch",draw) 
  
  elseif phase=="did" then

  	physics.start()
  
  end
  
end

function scene:hide( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
  	draw:setOff()
     
  elseif phase == "did" then
    
  end 
end

function scene:destroy( event )
  local sceneGroup = self.view

  audio.stop(2)

  display.remove(board)
  display.remove(downWall)
  display.remove(rightWall)
  display.remove(leftWall)
  display.remove(topWall)
  draw:erase()
  draw=nil

  composer.removeScene("pauseMenu")

  
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
return scene
