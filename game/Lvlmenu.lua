
local composer = require ("composer")
local scene = composer.newScene()
local widget = require("widget")

local backBtn
local backText

local loader=require("loader")

levelNumber=0
levelString=""


local levels=loader.loadValue("levels")


local function toLevel(event) --da riscrivere 

  local str=event.target.id
  local x=str:sub(#str,#str)
  local num=tonumber(x)

  levelNumber=num
  levelString="level"..tostring(num) 

  if loader.isLevelUnlocked(levels, num) then
     composer.gotoScene(levelString)
     audio.play(buttonSound, {channel=32})
     audio.stop(1)
     composer.removeScene("menu")
     composer.removeScene("Lvlmenu")
  end
end

local function backToMenu()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene("menu")
  composer.removeScene("Lvlmenu")
  end

local function lock()
  local xn=100
  local yn=100
  local group=display.newGroup()

  for i=1, #levels do
    if tonumber(levels:sub(i,i))==0 then
       local lck=widget.newButton{
          width=307,
          height=230,
          defaultFile="lock.png",
          x=xn,
          y=yn,
        }
       group:insert(lck)
    else  
       local unlck=display.newText(tostring(i),xn, yn, "orangeJuice.ttf",{fontSize=50})
             unlck:setFillColor(0,0,0)
             unlck.size=180
       group:insert(unlck)    
    end

    if xn<600 then
      xn=xn+250
    else
      xn=100
      yn=yn+250
    end

  
  end
  return group

end

local function buttons()
  local group=display.newGroup()
  local xn=100
  local yn=100

  for i=1, #levels do
    local btn= widget.newButton{
    width=180,
    height=180,
    defaultFile="nil.png",
    onRelease = toLevel,
    x=xn,
    y=yn,
    id="Level"..tostring(i)
    }
    group:insert(btn)
    if xn<600 then
      xn=xn+250
    else
      xn=100
      yn=yn+250
    end
  end

return group

end

  

function scene:create(event)
  local sceneGroup = self.view
  
  local background=display.newImageRect("background.png",4000,3000)


  backText=display.newText("Back",display.contentWidth-110,display.contentHeight-110,"orangeJuice.ttf",{fontSize=50})
  backText:setFillColor(0,0,0)
  backText.size=100
  
  local buttons=buttons()

  backBtn=widget.newButton{ 
  width=200,
  height=100,
  defaultFile="nil.png", 
  onRelease = backToMenu
  }
  backBtn.x=display.contentWidth-100
  backBtn.y=display.contentHeight-100
  backBtn.name="backbutton2"


  local locks=lock()

  
  sceneGroup:insert(background)
  sceneGroup:insert(backBtn)
  sceneGroup:insert(backText)
  sceneGroup:insert(locks)
  sceneGroup:insert(buttons)
  
  

end

function scene:show( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if phase == "will" then
    
  elseif phase == "did" then
  audio.play(menuSong, {loops=-1,channel=1})

  end 
end

function scene:hide( event )
  local sceneGroup = self.view
  local phase = event.phase
  


  if event.phase == "will" then
    
  
    
  elseif phase == "did" then

  end 
end

function scene:destroy( event )
  local sceneGroup = self.view

end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
return scene

