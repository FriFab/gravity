local composer = require ("composer")
local scene = composer.newScene()

local widget=require ("widget")
local graphics = require ("graphics")
local physics=require("physics")
physics.start()
physics.setScale(60)
physics.setDrawMode("normal")

local game=require("gameHandler") --gameHandler gestisce le dinamiche di vincita e perdita e altre cose comuni a tutti i livelli
local draw=require("Draw") --modulo per disegnare linee

local tiled = require "com.ponywolf.ponytiled" --copiare file ponytiled.lua in com/ponywolf
local mapData 
local map 


local downWall
local leftWall
local rightWall
local topWall

local gravWall
local seaWall

local Cx=display.contentCenterX 
local Cy=display.contentCenterY
local W=display.contentWidth
local H=display.contentHeight


local pla1
local pla2
local blueBtn
 

local start
local backBtn
local eraserBtn
local gravY=5


local function switchGravity() 
  local gx,gy=physics.getGravity()

  if(gy==gravY) then
    physics.setGravity(0,3)
  else
    physics.setGravity(5,gravY)
  end
end


  

local function onCollision(self, event)
 if event.phase=="began" and self.myName=="ball" then
   audio.play(game.bounceSound, {channel=31})
 end
 if event.phase=="began" and self.myName=="ball" and event.other.name=="grav" then
    switchGravity()
  end
 if event.phase=="began" and self.myName=="ball" and event.other.name=="bt" and not(game.lost) and not(game.won) then
   blueBtn:play()
   game:win()

 elseif (event.other.name=="top" or event.other.name=="left" or event.other.name=="right" or
  event.other.name=="sea") and (not game.lost) and (not game.won) and self.myName=="ball" then
 game:lose()

  end

end

local function startGame() 
  physics.addBody(game.ball,"dynamic",{density=1.0,friction=0.5, bounce=0.5,radius=35})
  Runtime:removeEventListener("touch", draw)
  display.remove(start)
  display.remove(eraserBtn)

end



local function pause()
audio.play(buttonSound, {channel=32})
physics.pause()
Runtime:removeEventListener("touch", draw)
composer.gotoScene("pauseMenu")

end


local function erase()

   draw:change()
end 


function scene:create(event)


local sceneGroup=self.view

physics.setGravity(0,9.8)

game:new()

game.ball=display.newImageRect("basketball.png",70,70) 

mapData= require "lvl3-map" -- load from lua export
map= tiled.new(mapData) -- look for images in lua export
game:setMap(map)


game.ball.x=100
game.ball.y=100
game.ball.myName="ball"



start=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="playButton.png", 
    onRelease=startGame
  }
backBtn=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="backButton.png", 
    onRelease=pause
  }

   eraserBtn = widget.newSwitch(
    {
        x = W-75,
        y = 185,
        style = "checkbox",
        width = 64,
        height = 64,
        onPress = erase,
        sheet = game.eraserSheet,
        frameOff = 2,
        frameOn = 1
    }
)

 start.x=W-75
 start.y=75

 backBtn.x=75
 backBtn.y=H-75

 blueBtn=display.newSprite(game.blueBtnSheet,game.blueBtnSequenceData)
 blueBtn.x=665
 blueBtn.y=920
 blueBtn.name="bt"

game.nextLvl=display.newImageRect("nil.png",400,100)
game.nextLvl.x=Cx
game.nextLvl.y=Cy+150





--Create screen boundaries
 leftWall = display.newRect(0,0,1, display.actualContentHeight*2 )
 rightWall = display.newRect(display.actualContentWidth, 350, 1, 760)
 topWall = display.newRect (0, 0, display.actualContentWidth*2, 1)
 downWall=display.newRect(0,display.actualContentHeight,display.actualContentWidth*2,1)
 gravWall=display.newRect(0,780,display.actualContentWidth*2,1)
 seaWall=display.newRect(display.actualContentWidth,870,1,280)


--Invisible walls
downWall.alpha=0
topWall.alpha=0
rightWall.alpha=0
leftWall.alpha=0
gravWall.alpha=0
seaWall.alpha=0


downWall.name="down"
topWall.name="top"
rightWall.name="right"
leftWall.name="left"
gravWall.name="grav"
seaWall.name="sea"




--Map objects
background=map:findObject("background")
pla1=map:findObject("plat1") 
pla2=map:findObject("plat2")
sea=map:findObject("sea")

sceneGroup:insert(map)
sceneGroup:insert(game.winText)
sceneGroup:insert(game.loseText)
sceneGroup:insert(game.nextText)
sceneGroup:insert(game.ball)
sceneGroup:insert(game.nextLvl)
sceneGroup:insert(blueBtn)
sceneGroup:insert(start)
sceneGroup:insert(backBtn)
sceneGroup:insert(eraserBtn)

audio.play(game.gameSong, {loops=-1, channel=2})

  physics.addBody (leftWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (rightWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (topWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody(downWall, "static", {density=0, friction=0.5, bounce=0.3}) 
  physics.addBody(gravWall, "static",{density=0, friction=0, bounce=0})
  physics.addBody(seaWall, "static",{density=0, friction=0, bounce=0})
  physics.addBody(pla1,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(pla2,"static", {density=0, friction=0.5, bounce=0.3})
  physics.addBody(blueBtn,"static",{density=0, friction=0.5, bounce=0.3, radius=15})

  gravWall.isSensor=true
  seaWall.isSensor=true

  draw:newLine()
  sceneGroup:insert(draw:setInk(40))

  game.ball.collision=onCollision
  game.ball:addEventListener("collision",game.ball)
  
  end 




function scene:show( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if phase == "will" then
    draw:setOn()
    Runtime:addEventListener("touch",draw) 
  
  elseif phase=="did" then

  physics.start()
  
  end
  
end

function scene:hide( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
    draw:setOff()
     
  elseif phase == "did" then
    
  end 
end

function scene:destroy( event )
  local sceneGroup = self.view
  audio.stop(2)
  draw:erase()
  draw=nil

  display.remove(leftWall)
  display.remove(rightWall)
  display.remove(topWall)
  display.remove(downWall)
  display.remove(seaWall)
  display.remove(gravWall)
  display.remove(blueBtn)
  display.remove(game.ball)

  composer.removeScene("pauseMenu")
  
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)


return scene




