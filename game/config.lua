--
-- For more information on config.lua see the Corona SDK Project Configuration Guide at:
-- https://docs.coronalabs.com/guide/basics/configSettings
--

application =
{

  content= 
  {
    width=700,
    height=980,
    fps=60,
    
    imageSuffix=
    {
    
       ["@2x"]=2,
       },
     },
     
  }
