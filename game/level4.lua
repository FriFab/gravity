local composer = require ("composer")
local scene = composer.newScene()

local widget=require ("widget")
local graphics = require ("graphics")
local physics=require("physics")

physics.start()
physics.setScale(60)
physics.setDrawMode("normal")

local game=require("gameHandler") --gameHandler gestisce le dinamiche di vincita e perdita e altre cose comuni a tutti i livelli
local draw=require("Draw") --modulo per disegnare linee
local tiled = require "com.ponywolf.ponytiled" --copiare file ponytiled.lua in com/ponywolf
local mapData 
local map 


local downWall
local leftWall
local rightWall
local topWall

local Cx=display.contentCenterX 
local Cy=display.contentCenterY
local W=display.contentWidth
local H=display.contentHeight


--parametri degli oggetti nel livello mancanti
local brick
local window1
local window2
local blueBtn

local start
local backBtn
local eraserBtn

local function onCollision(self, event)
 if event.phase=="began" and self.myName=="ball" then
   audio.play(game.bounceSound, {channel=31})
 end
 
 if event.phase=="began" and self.myName=="ball" and event.other.name=="w2" then
   transition.to(game.ball, {x = 560, y= 350, time=0})
 
 elseif event.phase=="began" and self.myName=="ball" and event.other.name=="bt" and not(game.lost) and not(game.won) then
   blueBtn:play()
   game:win()
 elseif (event.other.name=="top" or event.other.name=="left" or event.other.name=="right"
        or event.other.name=="down") and (not game.lost) and (not game.won) and self.myName=="ball" then
    game:lose()

  end

end

local function startGame() 
  physics.addBody(game.ball,"dynamic",{density=1.0,friction=0.5, bounce=0.5,radius=35})
  Runtime:removeEventListener("touch", draw)
  display.remove(start)
  display.remove(eraserBtn)

end


local function pause()
audio.play(buttonSound, {channel=32})
physics.pause()
Runtime:removeEventListener("touch", draw)
composer.gotoScene("pauseMenu")

end


local function erase()
   draw:change()
end 


function scene:create(event)


local sceneGroup=self.view

physics.setGravity(0,9.8)

game:new()

audio.play(game.gameSong, {loops=-1, channel=2})

game.ball=display.newImageRect("basketball.png",70,70) 

mapData= require "lvl4-map" -- load from lua export
map= tiled.new(mapData) -- look for images in lua export
game:setMap(map)


game.ball.x=100
game.ball.y=100
game.ball.myName="ball"

start=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="playButton.png", 
    onRelease=startGame
  }
backBtn=widget.newButton{ 
    width=100,
    height=100,
    defaultFile="backButton.png", 
    onRelease=pause
  }

   eraserBtn = widget.newSwitch(
    {
        x = W-75,
        y = 185,
        style = "checkbox",
        width = 64,
        height = 64,
        onPress = erase,
        sheet = game.eraserSheet,
        frameOff = 2,
        frameOn = 1
    }
)

 start.x=W-75
 start.y=75

 backBtn.x=75
 backBtn.y=H-75
 
 blueBtn=display.newSprite(game.blueBtnSheetR1,game.blueBtnSequenceDataR1)
 blueBtn.x=455
 blueBtn.y=945
 blueBtn.name="bt"

game.nextLvl=display.newImageRect("nil.png",400,100)
game.nextLvl.x=Cx
game.nextLvl.y=Cy+150
 
 
 --Create screen boundaries
 leftWall = display.newRect(0,0,1, display.actualContentHeight*2 )
 rightWall = display.newRect (display.actualContentWidth, 0, 1, display.actualContentHeight*2)
 topWall = display.newRect (0, 0, display.actualContentWidth*2, 1)
 downWall=display.newRect(0,display.actualContentHeight,display.actualContentWidth*2,1)


--Invisible walls
downWall.alpha=0
topWall.alpha=0
rightWall.alpha=0
leftWall.alpha=0

downWall.name="down"
topWall.name="top"
rightWall.name="right"
leftWall.name="left"


--obstacles
background=map:findObject("background")
window1 = map:findObject("w1")
window2 = map:findObject("w2") 
brick = {}
for i=2, 27 do
    brick[i] = map:findObject("b"..i+1)  
end


window1.name="w1"
window2.name="w2"


audio.play(game.gameSong, {loops=-1, channel=2})

  physics.addBody (leftWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (rightWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody (topWall, "static", { density=0, friction=0.5, bounce = 0.3} )
  physics.addBody(downWall, "static", {density=0, friction=0.5, bounce=0.3})   
  for i=2, 27 do
    physics.addBody(brick[i],"static", {density=1, friction=0.5, bounce=0.3})
  end
  
  physics.addBody(blueBtn,"static",{density=1, friction=0.5, bounce=0.3, radius=15})
  physics.addBody(window2,"static",{density=1, friction=0.5, bounce=0.3})
  window2.isSensor=true


  game.ball.collision=onCollision
  game.ball:addEventListener("collision",game.ball)


sceneGroup:insert(map)
sceneGroup:insert(game.winText)
sceneGroup:insert(game.loseText)
sceneGroup:insert(game.nextText)
sceneGroup:insert(game.ball)
sceneGroup:insert(game.nextLvl)
sceneGroup:insert(blueBtn)
sceneGroup:insert(start)
sceneGroup:insert(backBtn)
sceneGroup:insert(eraserBtn)


-- le seguenti istruzioni devono stare sempre alla fine della funzione
draw:newLine()
sceneGroup:insert(draw:setInk(40))


  
  end 


function scene:show( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if phase == "will" then
    draw:setOn()
    Runtime:addEventListener("touch",draw) 
  
  elseif phase=="did" then

  physics.start()
  
  end
  
end

function scene:hide( event )
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
    draw:setOff()
     
  elseif phase == "did" then
    
  end 
end

function scene:destroy( event )
  local sceneGroup = self.view
  audio.stop(2)
  draw:erase()
  draw=nil

  display.remove(downWall)
  display.remove(rightWall)
  display.remove(leftWall)
  display.remove(topWall)
  display.remove(blueBtn)
  display.remove(game.ball)

  composer.removeScene("pauseMenu")
  
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)


return scene

