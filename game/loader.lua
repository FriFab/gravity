
local loader={}

function loader.saveValue( strFilename, strValue )
	-- will save specified value to specified file
	local theFile = strFilename
	local theValue = strValue
	
	local path = system.pathForFile( theFile, system.DocumentsDirectory )
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "w+" )
	if file then
	   -- write game score to the text file
	   file:write( theValue )
	   io.close( file )
	end
end

--===================================================================================
--
-- Public Method: loadValue()	--> load single-line file and store it into variable
--
--===================================================================================

function loader.loadValue( strFilename )
	-- will load specified file, or create new file if it doesn't exist
	
	local theFile = strFilename
	
	local path = system.pathForFile( theFile, system.DocumentsDirectory )
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	if file then
	   -- read all contents of file into a string
	   local contents = file:read( "*a" )
	   io.close( file )
	   return contents
	else
	   -- create file b/c it doesn't exist yet
	   file = io.open( path, "w" )
	   file:write( "0" )
	   io.close( file )
	   return "0"
	end
end

function loader.isLevelUnlocked( strFileContent, lvl )

	local char=""

	for i=1, #strFileContent do
		if i==lvl then
			char=strFileContent:sub(i,i)
        end
    end

	if (tonumber(char)==1) then
		return true
	else
		return false
	end
end


return loader