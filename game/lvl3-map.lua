return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.17.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 10,
  height = 14,
  tilewidth = 70,
  tileheight = 70,
  nextobjectid = 57,
  properties = {},
  tilesets = {
    {
      name = "bg",
      firstgid = 1,
      tilewidth = 70,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "Platform tiles/Base pack/bg.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "tileset",
      firstgid = 10,
      tilewidth = 210,
      tileheight = 71,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 8,
      tiles = {
        {
          id = 0,
          image = "Platform tiles/Base pack/Tiles/boxAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 1,
          image = "Platform tiles/grass.png",
          width = 210,
          height = 70
        },
        {
          id = 2,
          image = "Platform tiles/Base pack/Items/cloud1.png",
          width = 128,
          height = 71
        },
        {
          id = 3,
          image = "Platform tiles/Base pack/Items/cloud2.png",
          width = 129,
          height = 71
        },
        {
          id = 4,
          image = "Platform tiles/Base pack/Items/bombFlash.png",
          width = 70,
          height = 70
        },
        {
          id = 5,
          image = "Platform tiles/Base pack/Tiles/liquidWaterTop.png",
          width = 70,
          height = 70
        },
        {
          id = 6,
          image = "Platform tiles/Base pack/Tiles/liquidWaterTop_mid.png",
          width = 70,
          height = 70
        },
        {
          id = 7,
          image = "Platform tiles/Base pack/Tiles/liquidWater.png",
          width = 70,
          height = 70
        }
      }
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "Background",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "background",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 980,
          width = 700,
          height = 980,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "Livello di oggetti 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 3,
          name = "plat1",
          type = "plat",
          shape = "rectangle",
          x = 70,
          y = 280,
          width = 210,
          height = 70,
          rotation = 0,
          gid = 11,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "sea",
          type = "sea",
          shape = "rectangle",
          x = 0,
          y = 980,
          width = 700,
          height = 210,
          rotation = 0,
          gid = 17,
          visible = true,
          properties = {}
        },
        {
          id = 40,
          name = "plat2",
          type = "plat",
          shape = "rectangle",
          x = 420,
          y = 630,
          width = 210,
          height = 70,
          rotation = 0,
          gid = 11,
          visible = true,
          properties = {}
        },
        {
          id = 41,
          name = "",
          type = "",
          shape = "rectangle",
          x = 140,
          y = 140,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 12,
          visible = true,
          properties = {}
        },
        {
          id = 42,
          name = "",
          type = "",
          shape = "rectangle",
          x = 490,
          y = 350,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 12,
          visible = true,
          properties = {}
        },
        {
          id = 45,
          name = "",
          type = "",
          shape = "rectangle",
          x = 420,
          y = 140,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 12,
          visible = true,
          properties = {}
        },
        {
          id = 46,
          name = "",
          type = "",
          shape = "rectangle",
          x = 70,
          y = 490,
          width = 129,
          height = 71,
          rotation = 0,
          gid = 13,
          visible = true,
          properties = {}
        },
        {
          id = 47,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 0,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 48,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 70,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 49,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 140,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 50,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 210,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 51,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 280,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 52,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 350,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 53,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 420,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 54,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 490,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 55,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 560,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        },
        {
          id = 56,
          name = "",
          type = "sea",
          shape = "rectangle",
          x = 630,
          y = 770,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 16,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
