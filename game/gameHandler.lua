
--Oggetto per gestire le dinamiche comuni a tutti i livelli--


local Cx=display.contentCenterX
local Cy=display.contentCenterY
local H=display.contentHeight
local W=display.contentWidth

local composer=require("composer")
local widget=require("widget")
local draw=require("Draw")
local loader=require("loader")


gameHandler= {


winningSound=audio.loadStream("win.wav"),
gameSong=audio.loadSound("game.wav"),
losingSound=audio.loadStream("lose.mp3"),
bounceSound=audio.loadSound("bounce.wav"),
boingSound=audio.loadSound("boing.mp3"),

winText,
loseText,
nextText,

nextLvl,

ball,
glass,
lost,
won,
map,
glassShape={65,-70, 23,90, -23,90, -65,-70},

boardsheet=graphics.newImageSheet("boardsheet.png",{
	width=70,
	height=70,
	numFrames=2,
	sheetContentWidth=70,
	sheetContentHeight=140
}),

boardSequenceData= { name="jump", frames={1, 2, 1}, time=300, loopCount=1 },

eraserSheet=graphics.newImageSheet( "eraser-sheet.png", {
    width = 256,
    height = 256,
    numFrames = 2,
    sheetContentWidth = 256,
    sheetContentHeight = 512
}),

blueBtnSheet=graphics.newImageSheet( "blueBtn-sheet.png", {
    width = 70,
    height = 70,
    numFrames = 2,
    sheetContentWidth = 70,
    sheetContentHeight = 140
    
}),

blueBtnSheetR1=graphics.newImageSheet( "blueBtn-sheet-r1.png", {
    width = 70,
    height = 70,
    numFrames = 2,
    sheetContentWidth = 140,
    sheetContentHeight = 70
    
}),

blueBtnSequenceData={name="jump", frames={1,2}, time=300, loopCount=1},
blueBtnSequenceDataR1={name="jump", frames={2,1}, time=300, loopCount=1},


boardShape= { 35,-15, 35,-5, -35,-5, -35,-15 },
spikeShape= { -35,35, -23.34,0, -11.67,35, 0,0, 11.66,35, 23.34,0, 35,35, 35,36, -35,36}

}


function gameHandler:new(o) 
    o = o or {}
    
    self.winText=display.newText("You Win!",Cx,Cy-50,"orangeJuice.ttf",{fontSize=50})
    self.loseText=display.newText("You Lose!",Cx,Cy-50,"orangeJuice.ttf",{fontSize=50})
    self.nextText=display.newText("Next Level",Cx,Cy+150,"orangeJuice.ttf",{fontSize=50})
    
    self.lost=false
    self.won=false

    self.winText:setFillColor(0,0,0)
    self.winText.size=150
    self.winText.alpha=0
    self.loseText:setFillColor(0,0,0)
    self.loseText.size=150
    self.loseText.alpha=0
    self.nextText:setFillColor(0,0,0)
    self.nextText.size=100
    self.nextText.alpha=0

    setmetatable(o,self)
    self.__index=self

	return o
end

local function nextLevel()
 composer.removeScene(levelString)
 audio.play(buttonSound, {channel=32})

	if(levelNumber<levelMax)
    then
    local num=levelNumber+1
    levelString="level"..num
	composer.gotoScene(levelString)
	levelNumber=num
    else
      audio.play(menuSong, {channel=1})
      composer.gotoScene("Lvlmenu")

    end
   
end


local function unlockLevel()
	local levels=loader.loadValue("levels")
    local str=levels:sub(1,levelNumber)

    if #str<#levels then
    str=str.."1"
      for i=#str, #levels-1 do
         str=str.."0"
      end
    loader.saveValue("levels",str)
    end
end

function gameHandler:win() 

audio.stop(2)
audio.play(self.winningSound, {channel=4})
self.winText.alpha=1
self.nextText.alpha=1
display.remove(self.glass)

self.nextLvl:addEventListener("touch", nextLevel)

draw:erase()
self:deleteObjects()
self.won=true
unlockLevel()


end

  function gameHandler:lose()

     audio.stop(2)
     audio.play(self.losingSound, {channel=4})
     self.loseText.alpha=1
     --physics.removeBody(draw)
     draw:erase()
     Runtime:removeEventListener("touch", draw)
     self.lost=true
     self:deleteObjects()
 end


function gameHandler:setMap(map)
	self.map=map
end


function gameHandler:deleteObjects() 
	local objs=self.map:listTypes("plat")
    print(#objs)
	for i=#objs, 1, -1 do
	   display.remove(objs[i])
    end

end


return gameHandler


