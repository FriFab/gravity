local composer = require ("composer")
local scene = composer.newScene()
local widget = require("widget")

local Cx=display.contentCenterX
local Cy=display.contentCenterY


local function play()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene(levelString)
end

local function restart()
  audio.play(buttonSound, {channel=32})
  composer.removeScene(levelString)
  composer.gotoScene(levelString)
 end

local function backToLvlMenu()
	audio.play(buttonSound, {channel=32})
	audio.play(menuSong, {channel=1})
	composer.removeScene(levelString)
	composer.gotoScene("Lvlmenu")
end





function scene:create(event)

	local sceneGroup=self.view

    local background
    if levelNumber==4 or levelNumber==6 then
    	background=display.newImageRect("bg-castle.png",4000,3000)
    	print("if")
    else
         background=display.newImageRect("background.png",4000,3000)
         print("else")
    end
  
	local playText=display.newText("Play",Cx,Cy-150,"orangeJuice.ttf", {fontSize=50})
    local backText=display.newText("Back to menu",Cx,Cy-50,"orangeJuice.ttf", {fontSize=50})
    local resText=display.newText("Restart",Cx,Cy+50,"orangeJuice.ttf", {fontSize=50})

    playText.size=100
    backText.size=100
    resText.size=100

    playText:setFillColor(0,0,0)
    backText:setFillColor(0,0,0)
    resText:setFillColor(0,0,0)


	local button1 = widget.newButton{
    width=200,
    height=100,
    defaultFile="nil.png",
    onRelease=play
   }
  button1.x=Cx
  button1.y=Cy-150

  local button2= widget.newButton{
    width=600,
    height=100,
    defaultFile="nil.png",
    onRelease=backToLvlMenu
   }
  button2.x=Cx
  button2.y=Cy-50

  local button3= widget.newButton{
    width=400,
    height=100,
    defaultFile="nil.png",
    onRelease=restart
   }
  button3.x=Cx
  button3.y=Cy+50

  sceneGroup:insert(background)
  sceneGroup:insert(button1)
  sceneGroup:insert(button2)
  sceneGroup:insert(button3)
  sceneGroup:insert(playText)
  sceneGroup:insert(backText)
  sceneGroup:insert(resText)

end





function scene:show(event)


end




function scene:hide(event)

end




function scene:destroy(event)

end




scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene