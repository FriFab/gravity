return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.17.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 10,
  height = 14,
  tilewidth = 70,
  tileheight = 70,
  nextobjectid = 78,
  properties = {},
  tilesets = {
    {
      name = "bg",
      firstgid = 1,
      tilewidth = 70,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "Platform tiles/Base pack/bg.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "platformer",
      firstgid = 10,
      tilewidth = 914,
      tileheight = 936,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 173,
      tiles = {
        {
          id = 0,
          image = "Platform tiles/Base pack/Tiles/box.png",
          width = 70,
          height = 70
        },
        {
          id = 1,
          image = "Platform tiles/Base pack/Tiles/boxAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 2,
          image = "Platform tiles/Base pack/Tiles/boxCoin.png",
          width = 70,
          height = 70
        },
        {
          id = 3,
          image = "Platform tiles/Base pack/Tiles/boxCoin_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 4,
          image = "Platform tiles/Base pack/Tiles/boxCoinAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 5,
          image = "Platform tiles/Base pack/Tiles/boxCoinAlt_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 6,
          image = "Platform tiles/Base pack/Tiles/boxEmpty.png",
          width = 70,
          height = 70
        },
        {
          id = 7,
          image = "Platform tiles/Base pack/Tiles/boxExplosive.png",
          width = 70,
          height = 70
        },
        {
          id = 8,
          image = "Platform tiles/Base pack/Tiles/boxExplosive_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 9,
          image = "Platform tiles/Base pack/Tiles/boxExplosiveAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 10,
          image = "Platform tiles/Base pack/Tiles/boxItem.png",
          width = 70,
          height = 70
        },
        {
          id = 11,
          image = "Platform tiles/Base pack/Tiles/boxItem_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 12,
          image = "Platform tiles/Base pack/Tiles/boxItemAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 13,
          image = "Platform tiles/Base pack/Tiles/boxItemAlt_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 14,
          image = "Platform tiles/Base pack/Tiles/boxWarning.png",
          width = 70,
          height = 70
        },
        {
          id = 15,
          image = "Platform tiles/Base pack/Tiles/brickWall.png",
          width = 70,
          height = 70
        },
        {
          id = 16,
          image = "Platform tiles/Base pack/Tiles/bridge.png",
          width = 70,
          height = 70
        },
        {
          id = 17,
          image = "Platform tiles/Base pack/Tiles/bridgeLogs.png",
          width = 70,
          height = 70
        },
        {
          id = 18,
          image = "Platform tiles/Base pack/Tiles/castle.png",
          width = 70,
          height = 70
        },
        {
          id = 19,
          image = "Platform tiles/Base pack/Tiles/castleCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 20,
          image = "Platform tiles/Base pack/Tiles/castleCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 21,
          image = "Platform tiles/Base pack/Tiles/castleCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 22,
          image = "Platform tiles/Base pack/Tiles/castleCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 23,
          image = "Platform tiles/Base pack/Tiles/castleCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 24,
          image = "Platform tiles/Base pack/Tiles/castleCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 25,
          image = "Platform tiles/Base pack/Tiles/castleHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 26,
          image = "Platform tiles/Base pack/Tiles/castleHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 27,
          image = "Platform tiles/Base pack/Tiles/castleHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 28,
          image = "Platform tiles/Base pack/Tiles/castleHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 29,
          image = "Platform tiles/Base pack/Tiles/castleHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 30,
          image = "Platform tiles/Base pack/Tiles/castleHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 31,
          image = "Platform tiles/Base pack/Tiles/castleHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 32,
          image = "Platform tiles/Base pack/Tiles/castleHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 33,
          image = "Platform tiles/Base pack/Tiles/castleLedgeLeft.png",
          width = 5,
          height = 22
        },
        {
          id = 34,
          image = "Platform tiles/Base pack/Tiles/castleLedgeRight.png",
          width = 5,
          height = 22
        },
        {
          id = 35,
          image = "Platform tiles/Base pack/Tiles/castleLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 36,
          image = "Platform tiles/Base pack/Tiles/castleMid.png",
          width = 70,
          height = 70
        },
        {
          id = 37,
          image = "Platform tiles/Base pack/Tiles/castleRight.png",
          width = 70,
          height = 70
        },
        {
          id = 38,
          image = "Platform tiles/Base pack/Tiles/dirt.png",
          width = 70,
          height = 70
        },
        {
          id = 39,
          image = "Platform tiles/Base pack/Tiles/dirtCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 40,
          image = "Platform tiles/Base pack/Tiles/dirtCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 41,
          image = "Platform tiles/Base pack/Tiles/dirtCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 42,
          image = "Platform tiles/Base pack/Tiles/dirtCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 43,
          image = "Platform tiles/Base pack/Tiles/dirtCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 44,
          image = "Platform tiles/Base pack/Tiles/dirtCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 45,
          image = "Platform tiles/Base pack/Tiles/dirtHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 46,
          image = "Platform tiles/Base pack/Tiles/dirtHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 47,
          image = "Platform tiles/Base pack/Tiles/dirtHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 48,
          image = "Platform tiles/Base pack/Tiles/dirtHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 49,
          image = "Platform tiles/Base pack/Tiles/dirtHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 50,
          image = "Platform tiles/Base pack/Tiles/dirtHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 51,
          image = "Platform tiles/Base pack/Tiles/dirtHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 52,
          image = "Platform tiles/Base pack/Tiles/dirtHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 53,
          image = "Platform tiles/Base pack/Tiles/dirtLedgeLeft.png",
          width = 5,
          height = 18
        },
        {
          id = 54,
          image = "Platform tiles/Base pack/Tiles/dirtLedgeRight.png",
          width = 5,
          height = 18
        },
        {
          id = 55,
          image = "Platform tiles/Base pack/Tiles/dirtLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 56,
          image = "Platform tiles/Base pack/Tiles/dirtMid.png",
          width = 70,
          height = 70
        },
        {
          id = 57,
          image = "Platform tiles/Base pack/Tiles/dirtRight.png",
          width = 70,
          height = 70
        },
        {
          id = 58,
          image = "Platform tiles/Base pack/Tiles/door_closedMid.png",
          width = 70,
          height = 70
        },
        {
          id = 59,
          image = "Platform tiles/Base pack/Tiles/door_closedTop.png",
          width = 70,
          height = 70
        },
        {
          id = 60,
          image = "Platform tiles/Base pack/Tiles/door_openMid.png",
          width = 70,
          height = 70
        },
        {
          id = 61,
          image = "Platform tiles/Base pack/Tiles/door_openTop.png",
          width = 70,
          height = 70
        },
        {
          id = 62,
          image = "Platform tiles/Base pack/Tiles/fence.png",
          width = 70,
          height = 70
        },
        {
          id = 63,
          image = "Platform tiles/Base pack/Tiles/fenceBroken.png",
          width = 70,
          height = 70
        },
        {
          id = 64,
          image = "Platform tiles/Base pack/Tiles/grass.png",
          width = 70,
          height = 70
        },
        {
          id = 65,
          image = "Platform tiles/Base pack/Tiles/grassCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 66,
          image = "Platform tiles/Base pack/Tiles/grassCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 67,
          image = "Platform tiles/Base pack/Tiles/grassCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 68,
          image = "Platform tiles/Base pack/Tiles/grassCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 69,
          image = "Platform tiles/Base pack/Tiles/grassCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 70,
          image = "Platform tiles/Base pack/Tiles/grassCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 71,
          image = "Platform tiles/Base pack/Tiles/grassHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 72,
          image = "Platform tiles/Base pack/Tiles/grassHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 73,
          image = "Platform tiles/Base pack/Tiles/grassHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 74,
          image = "Platform tiles/Base pack/Tiles/grassHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 75,
          image = "Platform tiles/Base pack/Tiles/grassHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 76,
          image = "Platform tiles/Base pack/Tiles/grassHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 77,
          image = "Platform tiles/Base pack/Tiles/grassHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 78,
          image = "Platform tiles/Base pack/Tiles/grassHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 79,
          image = "Platform tiles/Base pack/Tiles/grassLedgeLeft.png",
          width = 5,
          height = 24
        },
        {
          id = 80,
          image = "Platform tiles/Base pack/Tiles/grassLedgeRight.png",
          width = 5,
          height = 24
        },
        {
          id = 81,
          image = "Platform tiles/Base pack/Tiles/grassLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 82,
          image = "Platform tiles/Base pack/Tiles/grassMid.png",
          width = 70,
          height = 70
        },
        {
          id = 83,
          image = "Platform tiles/Base pack/Tiles/grassRight.png",
          width = 70,
          height = 70
        },
        {
          id = 84,
          image = "Platform tiles/Base pack/Tiles/hill_large.png",
          width = 48,
          height = 146
        },
        {
          id = 85,
          image = "Platform tiles/Base pack/Tiles/hill_largeAlt.png",
          width = 48,
          height = 146
        },
        {
          id = 86,
          image = "Platform tiles/Base pack/Tiles/hill_small.png",
          width = 48,
          height = 106
        },
        {
          id = 87,
          image = "Platform tiles/Base pack/Tiles/hill_smallAlt.png",
          width = 48,
          height = 106
        },
        {
          id = 88,
          image = "Platform tiles/Base pack/Tiles/ladder_mid.png",
          width = 70,
          height = 70
        },
        {
          id = 89,
          image = "Platform tiles/Base pack/Tiles/ladder_top.png",
          width = 70,
          height = 70
        },
        {
          id = 90,
          image = "Platform tiles/Base pack/Tiles/liquidLava.png",
          width = 70,
          height = 70
        },
        {
          id = 91,
          image = "Platform tiles/Base pack/Tiles/liquidLavaTop.png",
          width = 70,
          height = 70
        },
        {
          id = 92,
          image = "Platform tiles/Base pack/Tiles/liquidLavaTop_mid.png",
          width = 70,
          height = 70
        },
        {
          id = 93,
          image = "Platform tiles/Base pack/Tiles/liquidWater.png",
          width = 70,
          height = 70
        },
        {
          id = 94,
          image = "Platform tiles/Base pack/Tiles/liquidWaterTop.png",
          width = 70,
          height = 70
        },
        {
          id = 95,
          image = "Platform tiles/Base pack/Tiles/liquidWaterTop_mid.png",
          width = 70,
          height = 70
        },
        {
          id = 96,
          image = "Platform tiles/Base pack/Tiles/lock_blue.png",
          width = 70,
          height = 70
        },
        {
          id = 97,
          image = "Platform tiles/Base pack/Tiles/lock_green.png",
          width = 70,
          height = 70
        },
        {
          id = 98,
          image = "Platform tiles/Base pack/Tiles/lock_red.png",
          width = 70,
          height = 70
        },
        {
          id = 99,
          image = "Platform tiles/Base pack/Tiles/lock_yellow.png",
          width = 70,
          height = 70
        },
        {
          id = 100,
          image = "Platform tiles/Base pack/Tiles/rockHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 101,
          image = "Platform tiles/Base pack/Tiles/rockHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 102,
          image = "Platform tiles/Base pack/Tiles/ropeAttached.png",
          width = 70,
          height = 70
        },
        {
          id = 103,
          image = "Platform tiles/Base pack/Tiles/ropeHorizontal.png",
          width = 70,
          height = 70
        },
        {
          id = 104,
          image = "Platform tiles/Base pack/Tiles/ropeVertical.png",
          width = 70,
          height = 70
        },
        {
          id = 105,
          image = "Platform tiles/Base pack/Tiles/sand.png",
          width = 70,
          height = 70
        },
        {
          id = 106,
          image = "Platform tiles/Base pack/Tiles/sandCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 107,
          image = "Platform tiles/Base pack/Tiles/sandCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 108,
          image = "Platform tiles/Base pack/Tiles/sandCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 109,
          image = "Platform tiles/Base pack/Tiles/sandCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 110,
          image = "Platform tiles/Base pack/Tiles/sandCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 111,
          image = "Platform tiles/Base pack/Tiles/sandCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 112,
          image = "Platform tiles/Base pack/Tiles/sandHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 113,
          image = "Platform tiles/Base pack/Tiles/sandHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 114,
          image = "Platform tiles/Base pack/Tiles/sandHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 115,
          image = "Platform tiles/Base pack/Tiles/sandHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 116,
          image = "Platform tiles/Base pack/Tiles/sandHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 117,
          image = "Platform tiles/Base pack/Tiles/sandHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 118,
          image = "Platform tiles/Base pack/Tiles/sandHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 119,
          image = "Platform tiles/Base pack/Tiles/sandHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 120,
          image = "Platform tiles/Base pack/Tiles/sandLedgeLeft.png",
          width = 5,
          height = 18
        },
        {
          id = 121,
          image = "Platform tiles/Base pack/Tiles/sandLedgeRight.png",
          width = 5,
          height = 18
        },
        {
          id = 122,
          image = "Platform tiles/Base pack/Tiles/sandLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 123,
          image = "Platform tiles/Base pack/Tiles/sandMid.png",
          width = 70,
          height = 70
        },
        {
          id = 124,
          image = "Platform tiles/Base pack/Tiles/sandRight.png",
          width = 70,
          height = 70
        },
        {
          id = 125,
          image = "Platform tiles/Base pack/Tiles/sign.png",
          width = 70,
          height = 70
        },
        {
          id = 126,
          image = "Platform tiles/Base pack/Tiles/signExit.png",
          width = 70,
          height = 70
        },
        {
          id = 127,
          image = "Platform tiles/Base pack/Tiles/signLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 128,
          image = "Platform tiles/Base pack/Tiles/signRight.png",
          width = 70,
          height = 70
        },
        {
          id = 129,
          image = "Platform tiles/Base pack/Tiles/snow.png",
          width = 70,
          height = 70
        },
        {
          id = 130,
          image = "Platform tiles/Base pack/Tiles/snowCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 131,
          image = "Platform tiles/Base pack/Tiles/snowCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 132,
          image = "Platform tiles/Base pack/Tiles/snowCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 133,
          image = "Platform tiles/Base pack/Tiles/snowCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 134,
          image = "Platform tiles/Base pack/Tiles/snowCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 135,
          image = "Platform tiles/Base pack/Tiles/snowCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 136,
          image = "Platform tiles/Base pack/Tiles/snowHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 137,
          image = "Platform tiles/Base pack/Tiles/snowHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 138,
          image = "Platform tiles/Base pack/Tiles/snowHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 139,
          image = "Platform tiles/Base pack/Tiles/snowHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 140,
          image = "Platform tiles/Base pack/Tiles/snowHillLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 141,
          image = "Platform tiles/Base pack/Tiles/snowHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 142,
          image = "Platform tiles/Base pack/Tiles/snowHillRight.png",
          width = 70,
          height = 70
        },
        {
          id = 143,
          image = "Platform tiles/Base pack/Tiles/snowHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 144,
          image = "Platform tiles/Base pack/Tiles/snowLedgeLeft.png",
          width = 5,
          height = 18
        },
        {
          id = 145,
          image = "Platform tiles/Base pack/Tiles/snowLedgeRight.png",
          width = 5,
          height = 18
        },
        {
          id = 146,
          image = "Platform tiles/Base pack/Tiles/snowLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 147,
          image = "Platform tiles/Base pack/Tiles/snowMid.png",
          width = 70,
          height = 70
        },
        {
          id = 148,
          image = "Platform tiles/Base pack/Tiles/snowRight.png",
          width = 70,
          height = 70
        },
        {
          id = 149,
          image = "Platform tiles/Base pack/Tiles/stone.png",
          width = 70,
          height = 70
        },
        {
          id = 150,
          image = "Platform tiles/Base pack/Tiles/stoneCenter.png",
          width = 70,
          height = 70
        },
        {
          id = 151,
          image = "Platform tiles/Base pack/Tiles/stoneCenter_rounded.png",
          width = 70,
          height = 70
        },
        {
          id = 152,
          image = "Platform tiles/Base pack/Tiles/stoneCliffLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 153,
          image = "Platform tiles/Base pack/Tiles/stoneCliffLeftAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 154,
          image = "Platform tiles/Base pack/Tiles/stoneCliffRight.png",
          width = 70,
          height = 70
        },
        {
          id = 155,
          image = "Platform tiles/Base pack/Tiles/stoneCliffRightAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 156,
          image = "Platform tiles/Base pack/Tiles/stoneHalf.png",
          width = 70,
          height = 70
        },
        {
          id = 157,
          image = "Platform tiles/Base pack/Tiles/stoneHalfLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 158,
          image = "Platform tiles/Base pack/Tiles/stoneHalfMid.png",
          width = 70,
          height = 70
        },
        {
          id = 159,
          image = "Platform tiles/Base pack/Tiles/stoneHalfRight.png",
          width = 70,
          height = 70
        },
        {
          id = 160,
          image = "Platform tiles/Base pack/Tiles/stoneHillLeft2.png",
          width = 70,
          height = 70
        },
        {
          id = 161,
          image = "Platform tiles/Base pack/Tiles/stoneHillRight2.png",
          width = 70,
          height = 70
        },
        {
          id = 162,
          image = "Platform tiles/Base pack/Tiles/stoneLedgeLeft.png",
          width = 5,
          height = 24
        },
        {
          id = 163,
          image = "Platform tiles/Base pack/Tiles/stoneLedgeRight.png",
          width = 5,
          height = 24
        },
        {
          id = 164,
          image = "Platform tiles/Base pack/Tiles/stoneLeft.png",
          width = 70,
          height = 70
        },
        {
          id = 165,
          image = "Platform tiles/Base pack/Tiles/stoneMid.png",
          width = 70,
          height = 70
        },
        {
          id = 166,
          image = "Platform tiles/Base pack/Tiles/stoneRight.png",
          width = 70,
          height = 70
        },
        {
          id = 167,
          image = "Platform tiles/Base pack/Tiles/stoneWall.png",
          width = 70,
          height = 70
        },
        {
          id = 168,
          image = "Platform tiles/Base pack/Tiles/tiles_spritesheet.png",
          width = 914,
          height = 936
        },
        {
          id = 169,
          image = "Platform tiles/Base pack/Tiles/tochLit.png",
          width = 70,
          height = 70
        },
        {
          id = 170,
          image = "Platform tiles/Base pack/Tiles/tochLit2.png",
          width = 70,
          height = 70
        },
        {
          id = 171,
          image = "Platform tiles/Base pack/Tiles/torch.png",
          width = 70,
          height = 70
        },
        {
          id = 172,
          image = "Platform tiles/Base pack/Tiles/window.png",
          width = 70,
          height = 70
        }
      }
    },
    {
      name = "boxes",
      firstgid = 183,
      tilewidth = 70,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 15,
      tiles = {
        {
          id = 0,
          image = "Platform tiles/Base pack/Tiles/box.png",
          width = 70,
          height = 70
        },
        {
          id = 1,
          image = "Platform tiles/Base pack/Tiles/boxAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 2,
          image = "Platform tiles/Base pack/Tiles/boxCoin.png",
          width = 70,
          height = 70
        },
        {
          id = 3,
          image = "Platform tiles/Base pack/Tiles/boxCoin_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 4,
          image = "Platform tiles/Base pack/Tiles/boxCoinAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 5,
          image = "Platform tiles/Base pack/Tiles/boxCoinAlt_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 6,
          image = "Platform tiles/Base pack/Tiles/boxEmpty.png",
          width = 70,
          height = 70
        },
        {
          id = 7,
          image = "Platform tiles/Base pack/Tiles/boxExplosive.png",
          width = 70,
          height = 70
        },
        {
          id = 8,
          image = "Platform tiles/Base pack/Tiles/boxExplosive_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 9,
          image = "Platform tiles/Base pack/Tiles/boxExplosiveAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 10,
          image = "Platform tiles/Base pack/Tiles/boxItem.png",
          width = 70,
          height = 70
        },
        {
          id = 11,
          image = "Platform tiles/Base pack/Tiles/boxItem_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 12,
          image = "Platform tiles/Base pack/Tiles/boxItemAlt.png",
          width = 70,
          height = 70
        },
        {
          id = 13,
          image = "Platform tiles/Base pack/Tiles/boxItemAlt_disabled.png",
          width = 70,
          height = 70
        },
        {
          id = 14,
          image = "Platform tiles/Base pack/Tiles/boxWarning.png",
          width = 70,
          height = 70
        }
      }
    },
    {
      name = "sfondi",
      firstgid = 198,
      filename = "../../Altro/tilesets/sfondi.tsx",
      tilewidth = 1024,
      tileheight = 512,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {
        {
          id = 1,
          image = "../../Altro/Platform tiles/Ice expansion/Tiles/tree.png",
          width = 70,
          height = 70
        },
        {
          id = 2,
          image = "../../Altro/Platform tiles/Mushroom expansion/Backgrounds/bg_castle.png",
          width = 1024,
          height = 512
        }
      }
    },
    {
      name = "nuvole",
      firstgid = 201,
      tilewidth = 129,
      tileheight = 71,
      spacing = 0,
      margin = 0,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 3,
      tiles = {
        {
          id = 0,
          image = "Platform tiles/Base pack/Items/cloud1.png",
          width = 128,
          height = 71
        },
        {
          id = 1,
          image = "Platform tiles/Base pack/Items/cloud2.png",
          width = 129,
          height = 71
        },
        {
          id = 2,
          image = "Platform tiles/Base pack/Items/cloud3.png",
          width = 129,
          height = 71
        }
      }
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "Livello di oggetti 2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 50,
          name = "background",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 980,
          width = 700,
          height = 980,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "Livello di oggetti 1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 43,
          name = "box1",
          type = "plat",
          shape = "rectangle",
          x = 351,
          y = 490,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 184,
          visible = true,
          properties = {}
        },
        {
          id = 57,
          name = "pla1",
          type = "plat",
          shape = "rectangle",
          x = 70,
          y = 280,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 91,
          visible = true,
          properties = {}
        },
        {
          id = 58,
          name = "pla2",
          type = "plat",
          shape = "rectangle",
          x = 140,
          y = 280,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 92,
          visible = true,
          properties = {}
        },
        {
          id = 60,
          name = "pla3",
          type = "plat",
          shape = "rectangle",
          x = 210,
          y = 280,
          width = 70,
          height = 70,
          rotation = 0,
          gid = 93,
          visible = true,
          properties = {}
        },
        {
          id = 71,
          name = "",
          type = "",
          shape = "rectangle",
          x = 490,
          y = 140,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 201,
          visible = true,
          properties = {}
        },
        {
          id = 72,
          name = "",
          type = "",
          shape = "rectangle",
          x = 420,
          y = 350,
          width = 129,
          height = 71,
          rotation = 0,
          gid = 203,
          visible = true,
          properties = {}
        },
        {
          id = 73,
          name = "",
          type = "",
          shape = "rectangle",
          x = 70,
          y = 490,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 201,
          visible = true,
          properties = {}
        },
        {
          id = 75,
          name = "",
          type = "",
          shape = "rectangle",
          x = 70,
          y = 70,
          width = 129,
          height = 71,
          rotation = 0,
          gid = 202,
          visible = true,
          properties = {}
        },
        {
          id = 76,
          name = "",
          type = "",
          shape = "rectangle",
          x = 490,
          y = 630,
          width = 128,
          height = 71,
          rotation = 0,
          gid = 201,
          visible = true,
          properties = {}
        },
        {
          id = 77,
          name = "",
          type = "",
          shape = "rectangle",
          x = 140,
          y = 770,
          width = 129,
          height = 71,
          rotation = 0,
          gid = 202,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
