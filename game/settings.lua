local composer = require ("composer")
local scene = composer.newScene()
local widget = require("widget")

local Cx=display.contentCenterX
local Cy=display.contentCenterY

local musicText
local effText
local sysText
local backText

local Mtext
local Etext
local Stext


local function backToMenu()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene("menu")
  composer.removeScene("settings")
end

local function meno1()
  if(M>0) then
  M=M-1
  Mtext.text=tostring(M)
  audio.setVolume(M/5, {channel=1})
  audio.setVolume(M/5, {channel=2})

  end
end

local function piu1()
  if(M<5) then
    M=M+1
    Mtext.text=tostring(M)
    audio.setVolume(M/5, {channel=1})
    audio.setVolume(M/5, {channel=2})
  end
end

local function meno2()
  if(E>0) then
  E=E-1
  Etext.text=tostring(E)
  audio.setVolume(E/5, {channel=31})
  audio.setVolume(E/5, {channel=5})
  audio.setVolume(E/5, {channel=4})
  end
end

local function piu2()
  if(E<5) then
    E=E+1
    Etext.text=tostring(E)
    audio.setVolume(E/5, {channel=31})
    audio.setVolume(E/5, {channel=5})
    audio.setVolume(E/5, {channel=4})
  end
end

local function meno3()
  if(S>0) then
  S=S-1
  Stext.text=tostring(S)
  audio.setVolume(S/5, {channel=32})
  end
end

local function piu3()
  if(S<5) then
    S=S+1
    Stext.text=tostring(S)
    audio.setVolume(S/5, {channel=32})
  end
end





function scene:create(event)

  local sceneGroup=self.view


    local background=display.newImageRect("background.png",4000,3000)

    musicText=display.newText("Music        -        +",Cx,Cy-300,"orangeJuice.ttf", {fontSize=50})
    effText=display.newText("Effects     -        +",Cx,Cy-200,"orangeJuice.ttf", {fontSize=50})
    sysText=display.newText("System     -        +",Cx,Cy-100,"orangeJuice.ttf", {fontSize=50})
    backText=display.newText("Back",display.contentWidth-110,display.contentHeight-110,"orangeJuice.ttf",{fontSize=50})

    musicText.size=100
    effText.size=100
    sysText.size=100
    backText.size=100

    musicText:setFillColor(0,0,0)
    effText:setFillColor(0,0,0)
    sysText:setFillColor(0,0,0)
    backText:setFillColor(0,0,0)

    Mtext=display.newText(tostring(M),Cx+180,Cy-300,"orangeJuice.ttf", {fontSize=50})
    Etext=display.newText(tostring(E),Cx+180,Cy-200,"orangeJuice.ttf", {fontSize=50})
    Stext=display.newText(tostring(S),Cx+180,Cy-100,"orangeJuice.ttf", {fontSize=50})

    Mtext.size=100
    Etext.size=100
    Stext.size=100
    Etext:setFillColor(0,0,0)
    Mtext:setFillColor(0,0,0)
    Stext:setFillColor(0,0,0)



  local backBtn=widget.newButton{ 
  width=200,
  height=100,
  defaultFile="nil.png", 
  onRelease = backToMenu
  }
  backBtn.x=display.contentWidth-100
  backBtn.y=display.contentHeight-100

  local meno1=widget.newButton{
  x=Cx+100,
  y=Cy-300,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= meno1
}

  local piu1=widget.newButton{
  x=Cx+260,
  y=Cy-300,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= piu1
}

  local meno2=widget.newButton{
  x=Cx+100,
  y=Cy-200,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= meno2
}

  local piu2=widget.newButton{
  x=Cx+260,
  y=Cy-200,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= piu2
}

  local meno3=widget.newButton{
  x=Cx+100,
  y=Cy-100,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= meno3
}

  local piu3=widget.newButton{
  x=Cx+260,
  y=Cy-100,
  width=60,
  height=60,
  defaultFile="nil.png",
  onRelease= piu3
}



  sceneGroup:insert(background)
  sceneGroup:insert(backBtn)
  sceneGroup:insert(musicText)
  sceneGroup:insert(effText)
  sceneGroup:insert(sysText)
  sceneGroup:insert(backText)
  sceneGroup:insert(Mtext)
  sceneGroup:insert(Etext)
  sceneGroup:insert(Stext)
  sceneGroup:insert(piu1)
  sceneGroup:insert(meno1)
  sceneGroup:insert(piu2)
  sceneGroup:insert(meno2)
  sceneGroup:insert(piu3)
  sceneGroup:insert(meno3)

end





function scene:show(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
    Mtext.alpha=1
    Etext.alpha=1
    Stext.alpha=1
     
  elseif phase == "did" then
 
  end 

end




function scene:hide(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
    
     
  elseif phase == "did" then
    Mtext.alpha=0
    Etext.alpha=0
    Stext.alpha=0

    
  end 

end




function scene:destroy(event)
  local sceneGroup = self.view

  print("settings scene removed")

end




scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene