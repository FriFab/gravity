
local composer = require "composer"
local scene = composer.newScene()

local widget = require "widget"

menuSong=audio.loadSound("menu.mp3")
buttonSound=audio.loadSound("buttonSound.mp3")

local gameText
local playText
local settingsText
local aboutText

local function onPlayBtnRelease()
   audio.play(buttonSound, {channel=32})
   composer.gotoScene("Lvlmenu")

end

local function settings()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene("settings")
end

local function about()
  audio.play(buttonSound, {channel=32})
  composer.gotoScene("about")
end


function scene:create(event)
   local sceneGroup = self.view
   local playBtn
   
   local background=display.newImageRect("background.png",4000,3000)

  local menuBall=display.newImageRect("menuBall.png",160,160)
  menuBall.x=display.contentCenterX
  menuBall.y=display.contentCenterY-125

   
   gameText=display.newText("Gravity Pool",display.contentCenterX,display.contentCenterY-350,"orangeJuice.ttf",{fontSize=50})
   gameText:setFillColor(0,0,0)
   gameText.size=130

   playText=display.newText("Play",display.contentCenterX,display.contentCenterY+50,"orangeJuice.ttf",{fontSize=50})
   playText:setFillColor(0,0,0)
   playText.size=100

   settingsText=display.newText("Settings",display.contentCenterX,display.contentCenterY+150,"orangeJuice.ttf",{fontSize=50})
   settingsText:setFillColor(0,0,0)
   settingsText.size=100


   aboutText=display.newText("About",display.contentCenterX,display.contentCenterY+250,"orangeJuice.ttf",{fontSize=50})
   aboutText:setFillColor(0,0,0)
   aboutText.size=100

   
   playBtn = widget.newButton{
   width=200,
   height=100,
   defaultFile = "nil.png",
   onRelease = onPlayBtnRelease
   }
   playBtn.x = display.contentCenterX
   playBtn.y = display.contentCenterY+50

   local settBtn=widget.newButton{
   width=400,
   height=100,
   defaultFile = "nil.png",
   onRelease = settings
   }
   settBtn.x = display.contentCenterX
   settBtn.y = display.contentCenterY+160

  
   local aboutBtn=widget.newButton{
   width=350,
   height=100,
   defaultFile = "nil.png",
   onRelease = about
   }
   aboutBtn.x = display.contentCenterX
   aboutBtn.y = display.contentCenterY+270

   
   sceneGroup:insert(background)
   sceneGroup:insert(playBtn)
   sceneGroup:insert(gameText)
   sceneGroup:insert(playText)
   sceneGroup:insert(settingsText)
   sceneGroup:insert(aboutText)
   sceneGroup:insert(menuBall)
   sceneGroup:insert(settBtn)
   sceneGroup:insert(playBtn)
   sceneGroup:insert(aboutBtn)

   audio.play(menuSong, {loops=-1,channel=1})
end

--------------------------------------------

function scene:show(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
   
  end
  
  if event.phase == "did" then


 
  end
  
end

----------------------------------------------

function scene:hide(event)
  local sceneGroup = self.view
  local phase = event.phase
  
  if event.phase == "will" then
  
  
  end
  
  if event.phase == "did" then
  
  
  end
  
end

----------------------------------------------

function scene:destroy(event)
  local sceneGroup = self.view
  
end


scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene





