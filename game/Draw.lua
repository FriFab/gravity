
-- Draw a line

Draw = {pix_hit={},
        state=0,
        inkLevel={},
        buffer=nil
        }

local graphics=require "graphics"

function Draw:newLine(o)
    o = o or {}
    setmetatable(Draw,self)
    --self.__index=self
    return self

end

function Draw:setInk(range)
   self.buffer=display.newGroup()
   local w = 400
   local h = 30
   local img = display.newImageRect("buffer.png",w,h)
   img.x = 250
   img.y = 30
   self.buffer:insert(img) 
 
   local j = img.x-(w/2)
   local prevX = j
   self.count = 1

   if range > 397 or range == nil then
     self.range=397
   else
    self.range=range
   end
   
   for i=self.range, 1, -1  do
    --print(i)
      self.inkLevel[i]=display.newLine(prevX,h,j,h)
      self.inkLevel[i]:setStrokeColor(0,0,0)
      self.inkLevel[i].strokeWidth = h-7
      self.buffer:insert(self.inkLevel[i])
      prevX = j
      --print(prevX)
      j=j+1
      
   end
   

   return self.buffer
end



function Draw:touch(event)

    local i = #self.pix_hit +1
    if(event.phase=="began") then
            self.prevX = event.x
            self.prevY = event.y
        
    elseif(event.phase=="moved") then
        local counter = 0
        if self.state == 0 and self.count < self.range then
           
            self.pix_hit[i] = display.newLine(self.prevX, self.prevY, event.x, event.y)
            print(self.prevX , event.x)
            self.pix_hit[i]:setStrokeColor(0,0,0)
          
           self.pix_hit[i].strokeWidth = 10
           physics.addBody(self.pix_hit[i], "static", { bounce = -1, density=0.3, friction=0.7,})
           self.prevX = event.x
           self.prevY = event.y
           i = i+1

           self.inkLevel[self.count].isVisible=false
           self.count = self.count + 1

        else
            if self.state == 1 then
           for j=#self.pix_hit, 1, -1 do
                   if (math.rad(math.abs(event.x - self.pix_hit[j].x)^2 + math.abs(event.y - self.pix_hit[j].y)^2) < 10)  then
                     self.pix_hit[j].isVisible=false
                     physics.removeBody(self.pix_hit[j])
                     table.remove(self.pix_hit, j)
                     
                     self.count=self.count - 1

                     self.inkLevel[self.count].isVisible=true
                     

                     
                  --print("ci sono")
                 end
            end
        end
        end
   elseif(event.phase=="ended") then
           display.remove(self.tempLine)
           self.tempLine=nil
    end
  
  


  return event
end

function Draw:erase()
    for i=#self.pix_hit, 1, -1 do
      display.remove(self.pix_hit[i]) 
      self.pix_hit[i]=nil   
     -- print(self.pix_hit[i])
    end   
    self.state=0
end

function Draw:change(event)
     --  print(self.state)
    if self.state==0 then self.state=1
    else self.state=0
    end
    return event
end

function Draw:setOn()
    for i=#self.pix_hit, 1, -1 do
      self.pix_hit[i].isVisible=true
    end
end

function Draw:setOff()
   for i=#self.pix_hit, 1, -1 do
      self.pix_hit[i].isVisible=false
    end
end

local function getLast()
   local i = 0
   for i=#self.inkLevel, 1, -1 do
    
      if self.inkLevel[i]==nil then print (i-1)
      end
   end

end


return Draw